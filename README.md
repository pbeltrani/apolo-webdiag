# Apolo-WEBDIAG
============

# Arquitetura Apolo

Este projeto é um fork do projeto Apolo-Base (https://bitbucket.org/teamolimpus/apolo-base.git).

# Licença
---------

Apolo-Base é um software de código aberto, você pode redistribuí-lo e/ou
modificá-lo conforme a licença Apache versão 2.0. Veja o arquivo LICENSE-Apache.txt
